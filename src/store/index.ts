import {legacy_createStore as create_store, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import { rootReducer } from "./reducers";

export const store = create_store(rootReducer, applyMiddleware(thunk))
