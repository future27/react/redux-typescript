import React, {useEffect} from "react";
import { useTypeSelector } from "../hooks/userTypeSelector";
import {useAction} from "../hooks/useAction";

const UserList: React.FC = () => {
    const {users, error, loading} = useTypeSelector(state => state.user);
    const {fetchUsers} = useAction()
    useEffect(() => {
        fetchUsers()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    if (loading) {
        return <h1>Here is loading...</h1>
    }
    if (error) {
        return <h1>Error!</h1>
    }

    return (
        <div>
            {users.map(user => <div key={user.id}>{user.name}</div> )}
        </div>
    )
}

export default UserList;
